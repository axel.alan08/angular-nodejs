import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/login/session.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavbarComponent implements OnInit {
  constructor(private router: Router, private sessionService: SessionService) {
    this.sessionService.getLocalStorage('loggedUser');
  }

  ngOnInit(): void {}

  logOut() {
    if (confirm('¿Sure to logout?')) {
      this.sessionService.isLogged = false;
      this.sessionService.clearLocalStorage();
      this.router.navigate(['/login/signIn']);
    }
  }

  get logStatus() {
    return this.sessionService.isLogged;
  }
}
