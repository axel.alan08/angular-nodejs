export interface UserResponse {
  code: string;
  message: null;
  success: boolean;
  data: User;
}

export interface User {
  role: string;
  _id: string;
  name: string;
  surname: string;
  email: string;
}

export interface PutUserReq {
  password: string;
  email: string;
  role: string;
}

export interface PutUserRes {
  code: string;
  message: null;
  success: boolean;
  data: Data;
}

export interface Data {
  role: string;
  _id: string;
  name: string;
  surname: string;
  email: string;
}
