export interface RegisterRequest {
  name: string;
  surname: string;
  email: string;
  password: string;
}
export interface RegisterResponse {
  code: string;
  message: null;
  success: boolean;
  data: Data;
}

export interface Data {
  role: string;
  _id: string;
  name: string;
  surname: string;
  email: string;
}
