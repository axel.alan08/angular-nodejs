export interface GetClothesRes {
  code: string;
  message: null;
  success: boolean;
  data: GetData;
}
export interface DeleteClotheRes {
  code: string;
  message: null;
  success: boolean;
  data: null;
}

export interface GetData {
  count: number;
  results: Clothes[];
}

export interface Clothes {
  _id: string;
  type: string;
  quantity: number;
  price: number;
  description: string;
  createdAt: Date;
  updatedAt: Date;
  __v: number;
}
export interface PostClotheReq {
  type: string;
  quantity: number;
  price: number;
  description: string;
}

export interface PostClotheRes {
  code: string;
  message: null;
  success: boolean;
  data: PostData;
}

export interface PostData {
  _id: string;
  type: string;
  quantity: number;
  price: number;
  description: string;
  createdAt: Date;
  updatedAt: Date;
  __v: number;
}

export interface PutClotheReq {
  quantity: number;
  price: number;
  description: string;
}

export interface PutClotheRes {
  code: string;
  message: null;
  success: boolean;
  data: PutData;
}

export interface PutData {
  _id: string;
  type: string;
  quantity: number;
  price: number;
  description: string;
  createdAt: Date;
  updatedAt: Date;
  __v: number;
}
