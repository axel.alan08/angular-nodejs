export interface UsersResponse {
  code: string;
  message: null;
  success: boolean;
  data: Data;
}

export interface Data {
  count: number;
  results: Users[];
}

export interface Users {
  role: string;
  _id: string;
  name: string;
  surname: string;
  email: string;
}

export interface DeleteUser {
  code: string;
  message: null;
  success: boolean;
  data: null;
}
