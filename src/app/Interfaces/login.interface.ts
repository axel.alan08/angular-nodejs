export interface LoginResponse {
  code: string;
  message: null;
  success: boolean;
  data: Data;
}

export interface Data {
  user: User;
  token: string;
}

export interface User {
  role: string;
  _id: string;
  name: string;
  surname: string;
  email: string;
}
export interface LoginRequest {
  email: string;
  password: string;
}
