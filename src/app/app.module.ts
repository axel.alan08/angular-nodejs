import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ErrorComponent } from './error/error.component';
import { SharedModule } from './shared/shared.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SessionService } from './login/session.service';
import { TokenInterceptor } from './token.interceptor';
import { UsersService } from './perfil/users/users.service';
import { ClothesService } from './perfil/clothes/clothes.service';

@NgModule({
  declarations: [AppComponent, HomeComponent, ErrorComponent],
  imports: [HttpClientModule, BrowserModule, AppRoutingModule, SharedModule],
  providers: [
    SessionService,
    UsersService,
    ClothesService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
