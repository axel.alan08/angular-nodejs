import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { SessionService } from './login/session.service';

@Injectable({
  providedIn: 'root',
})
export class IsLoggedGuard implements CanLoad {
  private isLogged!: boolean;

  constructor(private sessionService: SessionService, private router: Router) {}

  canLoad(
    route: Route,
    segments: UrlSegment[]
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (this.sessionService.isLogged) {
      return true;
    } else {
      return this.router.parseUrl('/login/signIn');
    }
  }
}
