import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SessionService } from '../session.service';

@Component({
  templateUrl: './sign-in.component.html',
  styleUrls: [],
})
export class SignInComponent implements OnInit {
  loginForm!: FormGroup;

  constructor(
    private sessionService: SessionService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.email, Validators.required])],
      password: ['', Validators.required],
    });
  }
  doSubmit() {
    const { email, password } = this.loginForm.value;
    const { email: controlEmail } = this.loginForm.controls;

    if (password === '' || email === '') {
      alert('Uncompleted required fields');
      return;
    }
    if (controlEmail.invalid) {
      alert("Unvalid email, a valid email is required: 'example@example.com'");
      return;
    }

    this.sessionService.doSignIn(email, password);
  }
}
