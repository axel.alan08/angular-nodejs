import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import {
  LoginRequest,
  LoginResponse,
  User,
} from '../Interfaces/login.interface';
import {
  RegisterRequest,
  RegisterResponse,
} from '../Interfaces/register.interface';

@Injectable({
  providedIn: 'root',
})
export class SessionService {
  isLogged: boolean = false;
  public loggedUser!: User;
  public token!: string;
  public users: User[] = [];

  constructor(private router: Router, private http: HttpClient) {}

  doSignUp(name: string, surname: string, password: string, email: string) {
    let registerBody: RegisterRequest = {
      name: name,
      surname: surname,
      email: email,
      password: password,
    };
    const dosignup = this.http
      .post<RegisterResponse>(
        'http://localhost:3000/auth/register',
        registerBody
      )
      .subscribe(
        (res) => {
          if (res.success === true) {
            console.log(res);
            alert('User created successfull');
            return this.router.navigateByUrl('/login/signIn');
          }
          return console.log(res);
        },
        (err) => {
          console.log(err);
          return alert(err.error.message);
        },
        () => {
          dosignup.unsubscribe();
          console.log('unsubscribed from auth/register');
        }
      );
  }

  doSignIn(email: string, password: string) {
    let loginBody: LoginRequest = {
      email: email,
      password: password,
    };
    const dosignin = this.http
      .post<LoginResponse>('http://localhost:3000/auth/login', loginBody)
      .subscribe(
        (res) => {
          if (res.success === true) {
            this.token = res.data.token;
            console.log(this.token);
            this.loggedUser = res.data.user;
            console.log(JSON.stringify(this.loggedUser));
            this.isLogged = true;
            this.router.navigateByUrl('/perfil');
            this.setLocalstorage('loggedUser', this.loggedUser);
            return console.log(res);
          }
        },
        (err) => {
          console.log(err);
          return alert(err.error.message);
        },
        () => {
          dosignin.unsubscribe();
          console.log('unsubscribed from auth/login');
        }
      );
  }

  setLocalstorage(key: string, data: any) {
    try {
      localStorage.setItem('logStatus', JSON.stringify(true));
      localStorage.setItem(key, JSON.stringify(data));
    } catch (err) {
      console.error('Error setting key on local storage', err);
    }
  }
  //comprueba si no existe logStatus en LocalStorage, lo setea como false
  getLocalStorage(key: string) {
    try {
      let logStatus = localStorage.getItem('logStatus');
      if (logStatus === null) {
        console.log('log status on LS null setting to false');
        this.isLogged = false;
      } else {
        this.isLogged = JSON.parse(logStatus);
      }
      this.loggedUser = JSON.parse(localStorage.getItem('loggedUser')!);
      return JSON.parse(localStorage.getItem(key)!);
    } catch (err) {
      console.error('Error getting key from local storage', err);
    }
  }
  clearLocalStorage(): void {
    try {
      localStorage.clear();
    } catch (err) {
      console.error('Error cleaning local storage', err);
    }
  }
}
