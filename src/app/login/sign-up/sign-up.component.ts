import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SessionService } from '../session.service';

@Component({
  templateUrl: './sign-up.component.html',
})
export class SignUpComponent implements OnInit {
  public registerForm!: FormGroup;
  private validationFailed!: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private sessionService: SessionService
  ) {}

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.required],
    });
  }

  doSubmit() {
    const { name, surname, password, email } = this.registerForm.value;
    const controlEmail = this.registerForm.controls.email;

    if (name === '' || surname === '' || password === '' || email === '') {
      alert('Uncompleted required fields');
      return;
    }
    this.sessionService.users.forEach((user) => {
      if (email === user.email) {
        alert('The email used has already an user registered');
        this.validationFailed = true;
        return;
      }
    });

    if (controlEmail.invalid) {
      alert("Unvalid email, a valid email is required: 'example@example.com'");
      return;
    }
    if (this.validationFailed === true) {
      console.log('cortar la ejecucion');
      return;
    }

    this.sessionService.doSignUp(name, surname, password, email);
  }
}
