import { Component, OnInit } from '@angular/core';
import { SessionService } from '../login/session.service';

@Component({
  templateUrl: './home.component.html',
  styles: [],
})
export class HomeComponent implements OnInit {
  public username: string = 'usuario';

  constructor(private sessionService: SessionService) {}

  ngOnInit(): void {
    if (this.sessionService.loggedUser) {
      this.username = this.sessionService.loggedUser.name;
    }
  }

  get logStatus() {
    return this.sessionService.isLogged;
  }
}
