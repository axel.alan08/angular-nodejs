import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PerfilRoutingModule } from './perfil-routing.module';
import { PerfilComponent } from './perfil.component';
import { UsersComponent } from './users/users.component';
import { ClothesComponent } from './clothes/clothes.component';
import { ClotheComponent } from './clothes/clothe/clothe.component';
import { UserComponent } from './users/user/user.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ClothesService } from './clothes/clothes.service';
import { UsersService } from './users/users.service';
import { TokenInterceptor } from '../token.interceptor';
import { ReactiveFormsModule } from '@angular/forms';
import { CreateClotheComponent } from './clothes/create-clothe/create-clothe.component';

@NgModule({
  providers: [
    ClothesService,
    UsersService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
  ],
  declarations: [
    PerfilComponent,
    UsersComponent,
    ClothesComponent,
    ClotheComponent,
    UserComponent,
    CreateClotheComponent,
  ],
  imports: [
    HttpClientModule,
    CommonModule,
    PerfilRoutingModule,
    ReactiveFormsModule,
  ],
})
export class PerfilModule {}
