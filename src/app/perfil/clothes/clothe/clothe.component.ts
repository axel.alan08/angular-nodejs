import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ClothesService } from '../clothes.service';

interface Clothes {
  _id: string;
  type: string;
  quantity: number;
  price: number;
  description: string;
}

@Component({
  templateUrl: './clothe.component.html',
  styleUrls: ['./clothe.component.css'],
})
export class ClotheComponent implements OnInit {
  editForm!: FormGroup;
  idSaved!: string;
  clotheChoosed!: Clothes;

  constructor(
    private clothesService: ClothesService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    //tomar id guardada del servicio al componente
    this.idSaved = this.clothesService.idSaved;
    //seleccionar usuario que coincida con la id guardada
    for (let clothe of this.dbClothes) {
      if (clothe._id === this.idSaved) {
        this.clotheChoosed = clothe;
      }
    }

    this.editForm = this.formBuilder.group({
      quantity: [this.clotheChoosed.quantity],
      price: [this.clotheChoosed.price],
      description: [this.clotheChoosed.description],
    });
  }

  deleteClothe(_id: string) {
    if (confirm('¿Sure to delete this clothe?')) {
      this.clothesService.idSaved = _id;
      this.clothesService.deleteClothe(_id);
    }
  }

  editClothe() {
    if (confirm('¿Save changes?')) {
      const { quantity, price, description } = this.editForm.value;

      this.clothesService.putClothe(
        this.clotheChoosed._id,
        quantity,
        price,
        description
      );
    }
  }

  get dbClothes() {
    return this.clothesService.dbClothes;
  }
}
