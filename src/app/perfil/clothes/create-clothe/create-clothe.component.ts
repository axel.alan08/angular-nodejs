import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClothesService } from '../clothes.service';

@Component({
  templateUrl: './create-clothe.component.html',
  styleUrls: ['./create-clothe.component.css'],
})
export class CreateClotheComponent implements OnInit {
  public postClotheForm!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private clothesService: ClothesService
  ) {}

  ngOnInit(): void {
    this.postClotheForm = this.formBuilder.group({
      type: ['', Validators.required],
      quantity: ['', Validators.required],
      price: ['', Validators.required],
      description: ['New clothe', Validators.required],
    });
  }

  doSubmit() {
    if (confirm('Add new Clothe?')) {
      const { type, quantity, price, description } = this.postClotheForm.value;

      if (
        type === '' ||
        quantity === '' ||
        price === '' ||
        description === ''
      ) {
        alert('Uncompleted required fields');
        return;
      }
      this.clothesService.postClothe(type, quantity, price, description);
    }
  }
}
