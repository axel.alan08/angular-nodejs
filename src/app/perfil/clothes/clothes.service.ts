import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  Clothes,
  DeleteClotheRes,
  GetClothesRes,
  PostClotheReq,
  PostClotheRes,
  PutClotheReq,
  PutClotheRes,
} from 'src/app/Interfaces/clothe.interface';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class ClothesService {
  public idSaved!: string;
  dbClothes: Clothes[] = [];
  public clotheChoosed!: Clothes;
  constructor(private http: HttpClient, private router: Router) {}

  getAllClothes() {
    const getallclothes = this.http
      .get<GetClothesRes>('http://localhost:3000/clothes')
      .subscribe(
        (res) => {
          console.log(res);
          if (res.success) {
            this.dbClothes = res.data.results;
            return console.log(res);
          }
          return console.log(res);
        },
        (err) => {
          console.log(err);
          return alert(err.error.message);
        },
        () => {
          getallclothes.unsubscribe();
          console.log('unsubscribed from get /clothes');
        }
      );
  }
  postClothe(
    type: string,
    quantity: number,
    price: number,
    description: string
  ) {
    let postClotheBody: PostClotheReq = {
      type: type,
      quantity: quantity,
      price: price,
      description: description,
    };
    const postclothe = this.http
      .post<PostClotheRes>('http://localhost:3000/clothes', postClotheBody)
      .subscribe(
        (res) => {
          if (res.success === true) {
            console.log(res);
            alert('Clothe/s added successfull');
            return this.router.navigate(['perfil/clothes']);
          }
          return console.log(res);
        },
        (err) => {
          console.log(err);
          return alert(err.error.message);
        },
        () => {
          postclothe.unsubscribe();
          console.log('unsubscribed from post /clothes');
        }
      );
  }
  deleteClothe(_id: string) {
    const deleteclothe = this.http
      .delete<DeleteClotheRes>(`http://localhost:3000/clothes/${this.idSaved}`)
      .subscribe(
        (res) => {
          console.log(res);
          if (res.success) {
            for (let i = 0; i < this.dbClothes.length; i++) {
              if (this.dbClothes[i]._id === _id) {
                this.dbClothes.splice(i, 1);
              }
            }
            console.log(this.dbClothes);
            this.router.navigate(['perfil/clothes']);
            return console.log(res);
          }
          return console.log(res);
        },
        (err) => {
          console.log(err);
          return alert(err.error.message);
        },
        () => {
          deleteclothe.unsubscribe();
          console.log('unsubscribed from delete /clothes/');
        }
      );
  }

  putClothe(_id: string, quantity: number, price: number, description: string) {
    let putBody: PutClotheReq = {
      quantity: quantity,
      price: price,
      description: description,
    };
    const putclothe = this.http
      .put<PutClotheRes>(
        `http://localhost:3000/clothes/${this.idSaved}`,
        putBody
      )
      .subscribe(
        (res) => {
          console.log(res);
          if (res.success) {
            for (let i = 0; i < this.dbClothes.length; i++) {
              if (this.dbClothes[i]._id === _id) {
                this.dbClothes.splice(i, 1);
              }
            }
            console.log(this.dbClothes);
            this.router.navigateByUrl('/perfil/clothes');
            return console.log(res);
          }
          return console.log(res);
        },
        (err) => {
          console.log(err);
          return alert(err.error.message);
        },
        () => {
          putclothe.unsubscribe();
          console.log('unsubscribed from put /clothes/');
        }
      );
  }
}
