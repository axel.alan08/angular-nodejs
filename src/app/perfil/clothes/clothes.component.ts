import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClothesService } from './clothes.service';

@Component({
  templateUrl: './clothes.component.html',
  styleUrls: ['./clothes.component.css'],
})
export class ClothesComponent implements OnInit {
  public sweaterImg: string =
    'https://i.pinimg.com/originals/f0/fb/62/f0fb622b50910c8852e557c67d0cdd09.jpg';
  public shirtImg: string =
    'https://images.giant-bicycles.com/b_white,c_pad,h_650,q_80/TRAVERSE-T-SHIRT_BLACK_01/TRAVERSE-T-SHIRT_BLACK_01.jpg';
  public jacketImg: string =
    'https://revivalofthemachine.com/blog/wp-content/uploads/2016/11/Handmade-jacket-back-cr-800x600.jpg';
  public pantsImg: string =
    'https://sonicflywear.com/wp-content/uploads/2020/05/Long-Freefly-Pants.jpg';

  constructor(private clothesService: ClothesService, private router: Router) {}

  ngOnInit(): void {
    this.clothesService.getAllClothes();
  }

  //pasar _id de la vestimenta a la que se quiere acceder al servicio y acceder a esta
  goView(id: string) {
    console.log(id);
    this.clothesService.idSaved = id;
    this.router.navigateByUrl('/perfil/clothes/clothe');
  }

  get dbClothes() {
    return this.clothesService.dbClothes;
  }
}
