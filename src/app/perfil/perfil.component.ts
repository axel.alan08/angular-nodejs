import { Component, OnInit } from '@angular/core';
import { User } from '../Interfaces/login.interface';
import { SessionService } from '../login/session.service';
import { UsersService } from './users/users.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
})
export class PerfilComponent implements OnInit {
  constructor(
    private sessionService: SessionService,
    private usersService: UsersService
  ) {}
  public loggedUser!: User;

  //Se define al usuario logeado acorde al usuario guardado en el servicio de sesión
  ngOnInit(): void {
    this.loggedUser = this.loggedUserSession;
    this.loggedUser = this.sessionService.getLocalStorage('loggedUser');
  }

  get loggedUserSession() {
    return this.sessionService.loggedUser;
  }
}
