import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/Interfaces/user.interface';
import { UsersService } from '../users.service';

@Component({
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit {
  private idSaved!: string;
  public userChoosed!: User;
  public editForm!: FormGroup;
  public editMode: boolean = false;

  constructor(
    private usersService: UsersService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.idSaved = this.usersService.idSaved; //tomar id guardada del servicio al componente
    //seleccionar usuario que coincida con la id guardada
    for (let user of this.dbUsers) {
      if (user._id === this.idSaved) {
        this.userChoosed = user;
      }
    }
    this.editForm = this.formBuilder.group({
      email: [this.userChoosed.email, Validators.email],
      role: [this.userChoosed.role],
      password: [''],
    });
  }
  //put user
  editUser() {
    if (confirm('¿Save changes?')) {
      const { email, role, password } = this.editForm.value;
      const controlEmail = this.editForm.controls.email;

      if (controlEmail.invalid) {
        alert(
          "Unvalid email, a valid email is required: 'example@example.com'"
        );
        return;
      }
      this.usersService.putUser(this.userChoosed._id, email, role, password);
    }
  }

  get dbUsers() {
    return this.usersService.dbUsers;
  }
}
