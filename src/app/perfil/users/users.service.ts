import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import {
  PutUserReq,
  PutUserRes,
  User,
} from 'src/app/Interfaces/user.interface';
import {
  DeleteUser,
  Users,
  UsersResponse,
} from 'src/app/Interfaces/users.interface';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  public idSaved!: string;
  public loggedUser$ = new EventEmitter<User>();
  public userChoosed!: User;
  public dbUsers: User[] = [];
  constructor(private http: HttpClient, private router: Router) {}

  getAllUsers() {
    const getallusers = this.http
      .get<UsersResponse>('http://localhost:3000/user')
      .subscribe(
        (res) => {
          console.log(res);
          if (res.success) {
            this.dbUsers = res.data.results;
            return console.log(res);
          }
          return console.log(res);
        },
        (err) => {
          console.log(err);
          return alert(err.error.message);
        },
        () => {
          getallusers.unsubscribe();
          console.log('unsubscribed from get /user');
        }
      );
  }
  deleteUser(_id: string) {
    const deleteuser = this.http
      .delete<DeleteUser>(`http://localhost:3000/user/${this.idSaved}`)
      .subscribe(
        (res) => {
          console.log(res);
          if (res.success) {
            for (let i = 0; i < this.dbUsers.length; i++) {
              if (this.dbUsers[i]._id === _id) {
                this.dbUsers.splice(i, 1);
              }
            }
            console.log(this.dbUsers);
            return console.log(res);
          }
          return console.log(res);
        },
        (err) => {
          console.log(err);
          return alert(err.error.message);
        },
        () => {
          deleteuser.unsubscribe();
          console.log('unsubscribed from delete /user/');
        }
      );
  }
  putUser(_id: string, email: string, role: string, password: string) {
    let putBody: PutUserReq = {
      password: password,
      email: email,
      role: role,
    };
    const putuser = this.http
      .put<PutUserRes>(`http://localhost:3000/user/${this.idSaved}`, putBody)
      .subscribe(
        (res) => {
          console.log(res);
          if (res.success) {
            for (let i = 0; i < this.dbUsers.length; i++) {
              if (this.dbUsers[i]._id === _id) {
                this.dbUsers.splice(i, 1);
              }
            }
            console.log(this.dbUsers);
            this.router.navigateByUrl('/perfil/users');
            return console.log(res);
          }
          return console.log(res);
        },
        (err) => {
          console.log(err);
          return alert(err.error.message);
        },
        () => {
          putuser.unsubscribe();
          console.log('unsubscribed from put /user/');
        }
      );
  }
}
