import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from './users.service';

@Component({
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
})
export class UsersComponent implements OnInit {
  constructor(private usersService: UsersService, private router: Router) {}

  ngOnInit(): void {
    this.usersService.getAllUsers();
  }

  //acceder a pagina que muestre solo usuario seleccionado
  goView(id: string) {
    console.log(id);
    this.usersService.idSaved = id;
    this.router.navigateByUrl('/perfil/users/user');
  }

  deleteUser(_id: string) {
    if (confirm('¿Sure to delete this user?')) {
      this.usersService.idSaved = _id;
      this.usersService.deleteUser(_id);
      this.router.navigate(['perfil']);
    }
  }
  get dbUsers() {
    return this.usersService.dbUsers;
  }
}
