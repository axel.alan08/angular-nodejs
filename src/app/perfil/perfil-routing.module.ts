import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClotheComponent } from './clothes/clothe/clothe.component';
import { ClothesComponent } from './clothes/clothes.component';
import { CreateClotheComponent } from './clothes/create-clothe/create-clothe.component';
import { PerfilComponent } from './perfil.component';
import { UserComponent } from './users/user/user.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  { path: '', component: PerfilComponent },
  { path: 'users', component: UsersComponent },
  { path: 'users/user', component: UserComponent },
  { path: 'clothes', component: ClothesComponent },
  { path: 'clothes/add', component: CreateClotheComponent },
  { path: 'clothes/clothe', component: ClotheComponent },
  { path: '**', redirectTo: '../error', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PerfilRoutingModule {}
